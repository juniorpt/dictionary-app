const rp = require('request-promise');
var config = require("../config/".concat(process.env.NODE_ENV).concat(".js"));

async function dictionary_service(req, res, next) {
    const headers = {};
    let response = {};
    headers['app_id'] = '36a0b8b9';
    headers['app_key'] = 'c9a5585b62492e7c59c338b62f0d38ca';

    const restClient = (headers) => {
        const options = {
            method: 'GET',
            uri: config.url.concat(req.body.lang).concat('/').concat(req.body.key),
            headers,
            json: true,
            resolveWithFullResponse: true
        };
        return rp(options);
    };
    
    try {
        response = await restClient(headers);
    } catch (error) {
        response = error;
    }
    res.payload = response;
    next();
}

module.exports = dictionary_service;