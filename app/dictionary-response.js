function dictionary_response(req, res) {
    const response = {};
    if (res.payload.statusCode == 200) {
        response['code'] = res.payload.statusCode;
        response['message'] = res.payload.body;
        formatResponse(response);
    } else {
        response['code'] = res.payload.statusCode;
        response['message'] = res.payload.message;
    }

    return res.status(res.payload.statusCode).send(response);
}

function formatResponse(res) {
    const response = {};
    const definitions = [];
    const examples = [];
    res.message.results.forEach(function (result, i) {
        result.lexicalEntries.forEach(function (lexicalEntries, i) {
            response['pronunciation'] = lexicalEntries.pronunciations[1].audioFile;
            lexicalEntries.entries.forEach(function (entries, i) {
                entries.senses.forEach(function (senses, i) {
                    try {
                        if(senses.definitions != null){
                            senses.definitions.forEach(function (definition, i) {
                                definitions.push(definition)
                            });
                        }
                        
                        if(senses.examples != null){
                            senses.examples.forEach(function (example, i) {
                                examples.push(example.text)
                            });
                        }
                    } catch (error) {
                    }
                });
            });
        });
    });

    response['word'] = res.message.id;
    response['definitions'] = definitions;
    response['examples'] = examples;
    res.message = response;
    return res;
}



module.exports = dictionary_response;