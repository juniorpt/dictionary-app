var router = require('express').Router();
const service = require('./dictionary-service');
const response = require('./dictionary-response');
var bodyParser = require('body-parser')

var jsonParser = bodyParser.json()

const middlewares = [
  service,
  response
];

router.post('/word', jsonParser, middlewares);

module.exports = router;