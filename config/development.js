module.exports = {
    name: 'api-dictionary-manager',
    port: 3000,
    url: 'https://od-api.oxforddictionaries.com:443/api/v2/entries/'
};