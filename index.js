var express = require('express');
var app = express();
var config = require("./config/".concat(process.env.NODE_ENV).concat(".js"));

app.use('/', require('./app/route'));

app.get('/health', (req, res) => {
    res.send(`${config.name} up and running on port ${config.port}`);
});

app.listen(config.port, function () {
    console.log('Listening on port 3000!');
});